from PyQt4 import QtGui, QtCore

class AudioTreeItem:
	def __init__(self, data, parent = None):
		self.parentItem = parent
		self.itemData = data
		self.childItems = []

	def parent(self):
		return self.parentItem

	def child(self, number):
		return self.childItems[number]

	def childCount(self):
		return len(self.childItems)

	def childNumber(self):
		if self.parentItem:
			return self.parentItem.childItems.indexOf(self)
		return None

	def columnCount(self):
		return len(self.itemData)

	def data(self, column):
		return self.itemData[column]

	def setData(self, column, value):
		if column < 0 or column >= len(self.itemData):
			return False
		itemData[column] = value
		return True

	def insertChildren(self, position, count, columns):
		if position < 0 or position > len(self.childItems):
			return False
		for row in xrange(count):
			item = AudioTreeItem([], self)
			self.childItems.append(position,item)
		return True

	def removeChildren(self, position, count):
		if position < 0 or position + count > len(self.childItems):
			return False
		for row in xrange(count):
			del self.childItems[position]
		return True

	def insertColumns(self, position, columns):
		if position < 0 or position > len(self.itemData):
			return False
		for column in xrange(columns):
			self.itemData.insert(position, QtCore.QVariant())

		for child in self.childItems:
			child.insertColumns(position, columns)


class AudioTreeModel(QtCore.QAbstractItemModel):
	def __init__(self, headers, data, parent = None):
		

		
