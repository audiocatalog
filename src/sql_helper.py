def get_last_insert_id(query):
	query.exec_("select last_insert_rowid()")
	query.next()
	return query.value(0).toString()
