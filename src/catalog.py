#!/usr/bin/python

import sys
from PyQt4 import QtGui
from mainwindow import CatalogMainWindow


app = QtGui.QApplication(sys.argv)
catalog = CatalogMainWindow()
catalog.show()
sys.exit(app.exec_())
