from PyQt4 import QtSql
from mutagen.easyid3 import EasyID3
from mutagen.id3 import ID3
import os
import fnmatch

def file_tree_walk(folder, meth):
	folder = os.path.abspath(folder)
	for file in [file for file in os.listdir(folder) if not file in [".", ".."]]:
		nfile = os.path.join(folder, file)
		if os.path.isdir(nfile):
			file_tree_walk(nfile, meth)
		else:
			meth(nfile)


class AudioDB:
	def __init__(self):
		pass

	def create_new(self, name, folder):
		self.artists_dict = {}
		self.albums_dict = {}

		self.name = name

		self.db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
		self.db.setDatabaseName(name)
		self.db.open()
		
		query = QtSql.QSqlQuery()
		query.exec_("drop table `artists`")
		query.exec_("create table `artists` (`artist_id` integer not null primary key autoincrement, `title` char(256) not null unique)")
		query.exec_("drop table `albums`")
		query.exec_("create table `albums` (`album_id` integer not null primary key autoincrement, `artist_id` integer not null, `year` char(10), `title` char(256) not null)")
		query.exec_("create unique index albums_index on albums(`artist_id`, `year`, `title`)")
		query.exec_("drop table `songs`")
		query.exec_("create table `songs` (`song_id` integer not null primary key autoincrement, `album_id` integer not null, `artist_id` integer not null,  `title` char(256) not null, `filename` char(1024) not null)")
		
		self.current_artist_id = 0

		self.scan(folder)
	def open_exists(self, name):
		pass

	def close(self):
		self.db.close()

	def scan(self, folder):
		file_tree_walk(folder, self.insert_song)

	def rescan(self, name):
		pass

	def insert_song(self,filename):
		if fnmatch.fnmatch(filename, "*.mp3"):
			try:
				currentSongID3 = EasyID3(filename)
			#	print currentSongID3.valid_keys
				try:
					artist = currentSongID3["artist"][0]
				except KeyError:
					artist = u"Unknown artist"
					currentSongID3["artist"] = u"Unknown artist"

				try:
					album = currentSongID3["album"][0]
				except KeyError:
					album = u"Unknown album"
					currentSongID3["album"] = u"Unknown album"

				try:
					year = currentSongID3["date"][0]
				except KeyError:
					year = ""
					currentSongID3["date"] = ""

				try:
					song = currentSongID3["title"][0]
				except KeyError:
					song = "Unknown"
					currentSongID3["title"] = "Unknown"
				
				currentSongID3.save()

				query = QtSql.QSqlQuery()
				
				if query.exec_("insert into artists (`title`) values ('%s')" % artist):
					query.exec_("select last_insert_rowid()")
					query.next()
					if not query.value(0).toString() in self.artists_dict:
						self.artists_dict[artist] = query.value(0).toString()

				if query.exec_("insert into albums (`artist_id`, `year`, `title`) values ('%s', '%s', '%s')" % (self.artists_dict[artist], year, album)):
					query.exec_("select last_insert_rowid()")
					query.next()
					if not query.value(0).toString() in self.albums_dict:
						self.albums_dict[album] = query.value(0).toString()
				query.exec_("insert into songs (`album_id`, `artist_id`, `title`, `filename`) values ('%s', '%s', '%s', '%s')" % (self.albums_dict[album], self.artists_dict[artist], song, filename))
				
			except: pass
		


	def get_artists(self):
		query = QtSql.QSqlQuery()
		query.exec_("select * from artists")
		artists = []
		while query.next():
			artists.append((query.values(0).toString(), query.values(1).toString()))
		return artists

	def get_albums(self, artist):
		query = QtSql.QSqlQuery()
		query.exec_("select * from albums")
		albums = []
		while query.next():
			albums.append((query.values(0).toString(), query.values(1).toString(), query.values(2).toString(), query.values(3).toString()))
		return albums


	def get_songs(self, artist, album):
		query = QtSql.QSqlQuery()
		query.exec_("select * from songs")
		songs = []
		while query.next():
			albums.append((query.values(0).toString(), query.values(1).toString(), query.values(2).toString(), query.values(3).toString(), query.values(4).toString()))
		return songs

	
	def delete_artist(self, artist_id):
		query = QtSql.QSqlQuery()
		query.exec_("delete from artists where `artist_id`=%s" % artist_id)
		query.exec_("delete from albums where `artist_id`=%s" % artists_id)
		query.exec_("delete from songs where `artists_id`=%s" % artists_id)

	def delete_year(self):
		pass

	def delete_album(self, album_id):
		query = QtSql.QSqlQuery()
		query.exec_("delete from albums where `album_id`=%s" % album_id)
		query.exec_("delete from songs where `album_id`=%s" % album_id)


	def delete_song(self, song_id):
		query = QtSql.QSqlQuery()
		query.exec_("delete from songs where `song_id`=%s" % song_id)


	def rename_artist(self, artist_id, new_title):
		query = QtSql.QSqlQuery()
		query.exec_("update artists set `title`=%s where `artist_id`=%s" % (new_title, artist_id))
		query.exec_("select filename from songs where `artist_id`=%s" % artist_id)
		while query.next():
			id3 = EasyID3(query.values(0).toString())
			id3["artist"] = new_title
			id3.save()


	def rename_year(self):
		pass

	def rename_album(self, album_id, new_title):
		query = QtSql.QSqlQuery()
		query.exec_("update albums set `title`=%s where `album_id`=%s" % (new_title, album_id))
		query.exec_("select filename from songs where `album_id`=%s" % album_id)
		while query.next():
			id3 = EasyID3(query.values(0).toString())
			id3["album"] = new_title
			id3.save()

	def rename_song(self, song_id, new_title):
		query = QtSql.QSqlQuery()
		query.exec_("update songs set `title`=%s where `song_id`=%s" % (new_title, song_id))
		query.exec_("select filename from songs where `song_id`=%s" % song_id)
		while query.next():
			id3 = EasyID3(query.values(0).toString())
			id3["title"] = new_title
			id3.save()




#--------------------------------------------------
# a = AudioDB()
# a.create_new("foo", "/home/miksayer/Files/Music/Hardcore/")
# #--------------------------------------------------
# # db = QtSql.QSqlDatabase.addDatabase("QSQLITE")
# # db.setDatabaseName("foo")
# # db.open()
# # 
# # query = QtSql.QSqlQuery()
# # query.exec_("select * from artists")
# # while (query.next()):
# # 	print query.value(0).toString()
# # 	print query.value(1).toString()
# #-------------------------------------------------- 
# # db.close()
# a.close()
#-------------------------------------------------- 
