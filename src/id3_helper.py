from mutagen.easyid3 import EasyID3

class ID3Reader:
	def __init__(self, filename):
		self.id3 = EasyID3(filename)
	
	def get_artist_title():
		return self.id3["artist"][0]

	def get_album_title():
		return self.id3["album"][0]


