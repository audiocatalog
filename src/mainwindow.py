from PyQt4 import QtGui, QtCore
from audiodb import AudioDB

class CatalogMainWindow(QtGui.QMainWindow):
	def __init__(self, parent=None):
		QtGui.QWidget.__init__(self, parent)
		# self.setGeometry(300, 300, 250, 150)
		self.setWindowTitle('Catalog')
		self.setWindowIcon(QtGui.QIcon('../icons/icon.png'))
		self.statusBar()
		self.create_menu()

	def create_menu(self):
		menu_bar = self.menuBar()

		# Menu File
		menu_file = menu_bar.addMenu('File')

		file_new = QtGui.QAction(QtGui.QIcon('../icons/new.png'), 'Create new database...', self)
		file_new.setStatusTip('Create new audio database')
		file_new.triggered.connect(self.act_create_new_db)
		menu_file.addAction(file_new)

		menu_file.addSeparator()


		file_exit = QtGui.QAction(QtGui.QIcon('../icons/exit.png'), 'Exit', self)
		file_exit.setShortcut('Ctrl+Q')
		file_exit.setStatusTip('Exit')
		file_exit.triggered.connect(self.act_exit)
		menu_file.addAction(file_exit)

		# Menu Edit
		menu_edit = menu_bar.addMenu('Edit')
		
		# Menu Help
		menu_help = menu_bar.addMenu('Help')
		help_help = QtGui.QAction(QtGui.QIcon('../icons/exit.png'), 'Help', self)
		help_help.setShortcut('F1')
		help_help.setStatusTip('Help')
		help_help.triggered.connect(self.act_help)
		menu_help.addAction(help_help)
		
		menu_help.addSeparator()

		help_about = QtGui.QAction(QtGui.QIcon('../icons/exit.png'), 'About...', self)
		help_about.setStatusTip('About')
		help_about.triggered.connect(self.act_about)
		menu_help.addAction(help_about)




				
	@QtCore.pyqtSlot()
	def act_exit(self):
		QtGui.QMessageBox.aboutQt(self)

	@QtCore.pyqtSlot()
	def act_create_new_db(self):
		a = AudioDB()
		a.create_new("foo", "/home/miksayer/Files/Music")

	@QtCore.pyqtSlot()
	def act_help(self):
		QtGui.QMessageBox.aboutQt(self)

	@QtCore.pyqtSlot()
	def act_about(self):
 		QtGui.QMessageBox.about(self, 'Catalog', 'Catalog, version 0.01')



	
